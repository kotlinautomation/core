plugins {
    id("org.jetbrains.kotlin.js")
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-js:${Versions.kotlin}")
    implementation("org.jetbrains.kotlinx:kotlinx-html:${Versions.kotlinxHtml}")
    implementation("org.jetbrains:kotlin-react:${Versions.kotlinReact}")
    implementation("org.jetbrains:kotlin-react-dom:${Versions.kotlinReact}")
    implementation("org.jetbrains:kotlin-styled:${Versions.kotlinStyled}")
    implementation(project(":common"))
}

kotlin {
    js {
        browser()
        nodejs()
    }
}