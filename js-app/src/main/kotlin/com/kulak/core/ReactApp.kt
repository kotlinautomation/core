package com.kulak.core

import com.kulak.core.constants.localUrl
import com.kulak.core.entities.WebDriverCacheCommon
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.div
import react.setState
import kotlinx.browser.window

interface AppState : RState {
    var drivers: Array<WebDriverCacheCommon>
}

private class App : RComponent<RProps, AppState>() {
    override fun componentWillMount() {
        this.loadFromServer()
    }

    override fun RBuilder.render() {
        div("mdc-layout-grid") {
            div("mdc-layout-grid__cell mdc-layout-grid__cell") {
                driverList(state.drivers) {
                }
            }
        }
    }

    override fun componentDidMount() {
        this.loadFromServer()
        window.setInterval(this::loadFromServer, 2000)
    }

    fun loadFromServer() {
        setState {
            drivers = DesktopDriverServiceXhr(localUrl).getDrivers()
        }
    }
}

fun RBuilder.app() = child(App::class) {
}
