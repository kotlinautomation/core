package com.kulak.core

import com.kulak.core.constants.localUrl
import com.kulak.core.entities.WebDriverCacheCommon
import com.kulak.core.entities.actions.Action
import kotlinx.html.js.onClickFunction
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.button
import react.dom.div
import react.dom.li
import react.dom.span
import react.dom.ul

interface WebDriverCacheJsListProps : RProps {
    var data: Array<WebDriverCacheCommon>
    var block: (WebDriverCacheCommon) -> Unit
}

class WebdriverListComponent : RComponent<WebDriverCacheJsListProps, RState>() {
    override fun RBuilder.render() {
        ul("mdc-list mdc-list--two-line") {
            props.data.forEach { item ->
                li("list-item col-sm-4 col-md-2") {
                    div("thumbnail") {
                        span("mdc-list-item__text") {
                            span("mdc-list-item__primary-text") { +item.browserName }
                            span("mdc-list-item__secondary-text") { +item.startedAt }
                        }
                        attrs {
                            onClickFunction = {
                                props.block(item)
                            }
                        }
                        button(classes = "btn") {
                            span { +"Quit Driver" }
                            attrs.onClickFunction = {
                                DesktopDriverServiceXhr(localUrl)
                                    .quitDriver(Action(item.sessionId))
                            }
                        }
                    }
                }
            }
        }
    }
}

fun RBuilder.driverList(data: Array<WebDriverCacheCommon>, block: (WebDriverCacheCommon) -> Unit) =
    child(WebdriverListComponent::class) {
        attrs.data = data
        attrs.block = block
    }
