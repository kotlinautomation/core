package com.kulak.core

import com.kulak.core.constants.BROWSER_NAME
import com.kulak.core.constants.localUrl
import com.kulak.core.entities.browsers.BrowserType
import kotlinx.html.js.onClickFunction
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.button
import react.dom.span

class StartDriverComponent : RComponent<RProps, RState>() {
    override fun RBuilder.render() {
        button {
            span { +"Start Driver" }
            attrs.onClickFunction = {
                DesktopDriverServiceXhr(localUrl)
                    .startDriver(mapOf(BROWSER_NAME to BrowserType.CHROME.browserName))
            }
        }
    }
}