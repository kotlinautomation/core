package com.kulak.core

import kotlinx.browser.document
import react.dom.render

fun main() {
    document.addEventListener("DOMContentLoaded", {
        render(document.getElementById("root")) {
            app()
        }
        render(document.getElementById("startDriver")) {
            child(StartDriverComponent::class) {}
        }
    })
}
