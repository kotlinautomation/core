plugins {
    kotlin("multiplatform")
    id("maven-publish")
}
repositories {
    mavenCentral()
}
kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        withJava()
    }
    js {
        browser()
        nodejs()
    }
    sourceSets {
        val commonMain  by getting
        getByName("jvmMain").apply {
            dependencies {
                implementation("com.fasterxml.jackson.module:jackson-module-kotlin:${Versions.jackson}")
                implementation("com.squareup.retrofit2:retrofit:${Versions.retrofit2}")
                implementation("com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit2}")
                implementation("com.squareup.retrofit2:converter-jackson:${Versions.retrofit2}")
                implementation("io.reactivex.rxjava2:rxjava:${Versions.rxJava}")
            }
        }
        val jsMain by getting
        val commonTest by getting
        val jsTest by getting
        val jvmTest by getting

    }
}