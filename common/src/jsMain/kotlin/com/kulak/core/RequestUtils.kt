package com.kulak.core

import com.kulak.core.entities.actions.Action
import org.w3c.dom.events.Event
import org.w3c.xhr.XMLHttpRequest

fun httpGet(theUrl: String, params: Map<String, Any> = mapOf(), handler: ((Event) -> dynamic)? = null): String {
    val xhr = XMLHttpRequest()
    val queries = params.map { "${it.key}=${it.value}" }.joinToString(separator = "&")
    xhr.open("GET", "$theUrl?$queries", false) // false for synchronous request
    xhr.onreadystatechange = handler
    xhr.send(null)
    return xhr.responseText
}

fun httpPost(theUrl: String, action: Action, handler: ((Event) -> dynamic)? = null): String {
    val xhr = XMLHttpRequest()
    xhr.open("POST", theUrl, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    val data = JSON.stringify(action)
    xhr.onreadystatechange = handler
    xhr.send(data)
    return xhr.responseText
}