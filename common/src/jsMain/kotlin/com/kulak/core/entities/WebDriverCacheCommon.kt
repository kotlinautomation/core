package com.kulak.core.entities

actual class WebDriverCacheCommon actual constructor(var browserName: String, var startedAt: String, var sessionId: String)