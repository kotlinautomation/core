package com.kulak.core

import com.kulak.core.constants.DRIVER_PATH
import com.kulak.core.constants.DriverEndPoints.LOGS
import com.kulak.core.constants.DriverEndPoints.OPEN_PAGE
import com.kulak.core.constants.DriverEndPoints.QUIT_DRIVER
import com.kulak.core.constants.DriverEndPoints.START_DRIVER
import com.kulak.core.entities.WebDriverCacheCommon
import com.kulak.core.entities.WebElementCacheCommon
import com.kulak.core.entities.actions.Action
import com.kulak.core.entities.actions.ActionResult
import com.kulak.core.entities.actions.ActionUrl
import org.w3c.dom.events.Event
import org.w3c.fetch.Request

class DesktopDriverServiceXhr(private val baseUrl: String) {
    fun quitDriver(action: Action, handler: ((Event) -> dynamic)? = null): ActionResult =
        JSON.parse(httpPost("$baseUrl/$DRIVER_PATH$QUIT_DRIVER", action, handler))

    fun openPage(actionUrl: ActionUrl, handler: ((Event) -> dynamic)? = null): ActionResult =
        JSON.parse(httpPost("$baseUrl/$DRIVER_PATH$OPEN_PAGE", actionUrl, handler))

    fun startDriver(
        capabilities: Map<String, String>,
        handler: ((Event) -> dynamic)? = null
    ): ActionResult =
        JSON.parse(httpGet("$baseUrl/$DRIVER_PATH$START_DRIVER", capabilities, handler))

    fun getLogs(action: Action, handler: ((Event) -> dynamic)? = null): Array<Any> =
        JSON.parse(httpPost("$baseUrl/$DRIVER_PATH$LOGS", action, handler))

    fun getDrivers(handler: ((Event) -> dynamic)? = null): Array<WebDriverCacheCommon> =
        JSON.parse(httpGet("$baseUrl/$DRIVER_PATH/", handler = handler))

    fun getDriver(sessionId: String, handler: ((Event) -> dynamic)? = null): WebDriverCacheCommon =
        JSON.parse(httpGet("$baseUrl/$DRIVER_PATH/$sessionId", handler = handler))

    fun getElements(sessionId: String, handler: ((Event) -> dynamic)? = null): Array<WebElementCacheCommon> =
        JSON.parse(httpGet("$baseUrl/$DRIVER_PATH/$sessionId/elements", handler = handler))
}