package com.kulak.core

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.kulak.core.constants.DRIVER_PATH
import com.kulak.core.constants.DriverEndPoints.GET_DRIVER
import com.kulak.core.constants.DriverEndPoints.GET_DRIVER_ELEMENTS
import com.kulak.core.constants.DriverEndPoints.LOGS
import com.kulak.core.constants.DriverEndPoints.OPEN_PAGE
import com.kulak.core.constants.DriverEndPoints.QUIT_DRIVER
import com.kulak.core.constants.DriverEndPoints.START_DRIVER
import com.kulak.core.constants.SESSION_ID
import com.kulak.core.entities.WebDriverCacheCommon
import com.kulak.core.entities.WebElementCacheCommon
import com.kulak.core.entities.actions.Action
import com.kulak.core.entities.actions.ActionResult
import com.kulak.core.entities.actions.ActionUrl
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

class DesktopDriverService(private val baseUrl: String) {
    fun quitDriver(action: Action): ActionResult =
        DriverService.create(baseUrl).quitDriver(action).execute().body()!!

    fun startDriver(capabilities: Map<String, String>): ActionResult =
        DriverService.create(baseUrl).startDriver(capabilities).execute().body()!!

    fun openPage(actionUrl: ActionUrl): ActionResult =
        DriverService.create(baseUrl).openPage(actionUrl).execute().body()!!

    fun getDrivers(): List<WebDriverCacheCommon> =
        DriverService.create(baseUrl).getDrivers().execute().body()!!.map { it }

    fun getElements(sessionId: String): List<WebElementCacheCommon> =
        DriverService.create(baseUrl).getElements(sessionId).execute().body()!!.map { it }

    fun getLogs(action: Action): Array<Any> =
        DriverService.create(baseUrl).getLogs(action).execute().body()!!

    fun getDriver(sessionId: String): WebDriverCacheCommon =
        DriverService.create(baseUrl).getDriver(sessionId).execute().body()!!
}

internal interface DriverService {
    @GET("$DRIVER_PATH$START_DRIVER")
    fun startDriver(
        @QueryMap capabilities: Map<String, String>
    ): Call<ActionResult>

    @POST("$DRIVER_PATH$QUIT_DRIVER")
    fun quitDriver(@Body action: Action): Call<ActionResult>

    @POST("$DRIVER_PATH$OPEN_PAGE")
    fun openPage(@Body actionUrl: ActionUrl): Call<ActionResult>

    @POST("$DRIVER_PATH$LOGS")
    fun getLogs(@Body action: Action): Call<Array<Any>>

    @GET("$DRIVER_PATH/")
    fun getDrivers(): Call<WebDriverCaches>

    @GET("$DRIVER_PATH$GET_DRIVER")
    fun getDriver(@Path(SESSION_ID) sessionId: String): Call<WebDriverCacheCommon>

    @GET("$DRIVER_PATH$GET_DRIVER_ELEMENTS")
    fun getElements(@Path(SESSION_ID) sessionId: String): Call<WebElementCaches>

    companion object Factory {
        fun create(baseUrl: String): DriverService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(jacksonObjectMapper()))
                .baseUrl(baseUrl)
                .build()
            return retrofit.create(DriverService::class.java);
        }
    }
}

class WebDriverCaches : ArrayList<WebDriverCacheCommon>()

class WebElementCaches : ArrayList<WebElementCacheCommon>()
