package com.kulak.core.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
actual class WebDriverCacheCommon actual constructor(var browserName: String, var startedAt: String, var sessionId: String)