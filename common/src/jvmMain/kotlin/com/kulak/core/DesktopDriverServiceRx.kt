package com.kulak.core

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.kulak.core.constants.DRIVER_PATH
import com.kulak.core.constants.DriverEndPoints.GET_DRIVER
import com.kulak.core.constants.DriverEndPoints.GET_DRIVER_ELEMENTS
import com.kulak.core.constants.DriverEndPoints.LOGS
import com.kulak.core.constants.DriverEndPoints.OPEN_PAGE
import com.kulak.core.constants.DriverEndPoints.QUIT_DRIVER
import com.kulak.core.constants.DriverEndPoints.START_DRIVER
import com.kulak.core.constants.SESSION_ID
import com.kulak.core.constants.localUrl
import com.kulak.core.entities.WebDriverCacheCommon
import com.kulak.core.entities.actions.Action
import com.kulak.core.entities.actions.ActionResult
import com.kulak.core.entities.actions.ActionUrl
import io.reactivex.Flowable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.QueryMap
import java.util.concurrent.TimeUnit

interface DriverServiceRx {
    @GET("$DRIVER_PATH$START_DRIVER")
    fun startDriver(@QueryMap capabilities: Map<String, Any>): Flowable<ActionResult>

    @POST("$DRIVER_PATH$QUIT_DRIVER")
    fun quitDriver(@Body action: Action): Flowable<ActionResult>

    @POST("$DRIVER_PATH$OPEN_PAGE")
    fun openPage(@Body actionUrl: ActionUrl): Flowable<ActionResult>

    @POST("$DRIVER_PATH$LOGS")
    fun getLogs(@Body action: Action): Flowable<Array<Any>>

    @GET("$DRIVER_PATH/")
    fun getDrivers(): Flowable<WebDriverCaches>

    @GET("$DRIVER_PATH$GET_DRIVER")
    fun getDriver(@Path(SESSION_ID) sessionId: String): Flowable<WebDriverCacheCommon>

    @GET("$DRIVER_PATH$GET_DRIVER_ELEMENTS")
    fun getElements(@Path(SESSION_ID) sessionId: String): Flowable<WebElementCaches>

    companion object Factory {
        fun create(baseUrl: String = localUrl): DriverServiceRx {
            val builder = OkHttpClient.Builder()
//                .addInterceptor(getLogInterceptor())
                .readTimeout(100000, TimeUnit.SECONDS)
                .connectTimeout(100000, TimeUnit.SECONDS)
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create(jacksonObjectMapper()))
                .baseUrl(baseUrl)
                .client(builder.build())
                .build()
            return retrofit.create(DriverServiceRx::class.java);
        }
    }
}