package com.kulak.core.entities.actions

open class Action(var sessionId: String = "")
class ActionUrl(sessionId: String, var url: String = "") : Action(sessionId)
open class ElementAction(sessionId: String, var locator: Locator = Locator(LocatorType.XPATH))
    : Action(sessionId)

class ElementActionWithText(sessionId: String, locator: Locator, var text: String)
    : ElementAction(sessionId, locator)
