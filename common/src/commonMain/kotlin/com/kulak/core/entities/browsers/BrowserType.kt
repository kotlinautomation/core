package com.kulak.core.entities.browsers

enum class BrowserType(var browserName: String) {
    CHROME("chrome"),
    FIREFOX("firefox");
}
