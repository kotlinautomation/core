package com.kulak.core.entities

expect class WebDriverCacheCommon(
    browserName: String,
    startedAt: String,
    sessionId: String
)

class WebElementCacheCommon(
    var locator: String
)