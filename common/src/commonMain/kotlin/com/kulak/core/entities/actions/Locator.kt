package com.kulak.core.entities.actions

class Locator(var type: LocatorType, var value: String = "")

enum class LocatorType {
    XPATH,
    CSS,
    ID
}
