package com.kulak.core.constants

const val localUrl = "http://localhost:8090"
const val DRIVER_PATH = "api/driver"
const val ELEMENT_PATH = "api/element"
const val BROWSER_NAME = "browserName"
const val SESSION_ID = "sessionId"

object DriverEndPoints {
    const val START_DRIVER = "/startDriver"
    const val QUIT_DRIVER = "/quitDriver"
    const val OPEN_PAGE = "/openPage"
    const val LOGS = "/logs"
    const val GET_DRIVER = "/{$SESSION_ID}"
    const val GET_DRIVER_ELEMENTS = "$GET_DRIVER/elements"
}

object ElementEndPoints {
    const val IS_ELEMENT_PRESENT = "/isElementPresent"
    const val WAIT_FOR_ELEMENT_PRESENT = "/waitForElementPresent"
    const val CLICK = "/click"
    const val TYPE = "/type"
    const val GET_TEXT = "/getText"
    const val GET_ELEMENT = "/{locator}"
}