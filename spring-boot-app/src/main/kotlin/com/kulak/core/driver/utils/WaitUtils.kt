package com.kulak.core.driver.utils

import com.kulak.core.driver.constants.Timeout
import com.kulak.core.entities.actions.Locator
import org.awaitility.kotlin.await
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriverException

open class DriverWait(private var webDriver: WebDriver) {
    fun isPageLoaded() = webDriver.executeScript("return document.readyState") == "complete"
}

class ElementWait(private var webDriver: WebDriver, private var locator: Locator? = null) {
    fun isElementPresent(): Boolean =
        webDriver.findElements(locator?.by).any { it.isDisplayed }
}

fun waitForElementPresent(
    driver: WebDriver,
    locator: Locator,
    timeout: Timeout = Timeout.S_30,
    pollInterval: Timeout = Timeout.MS_250
): ElementWait = ElementWait(driver, locator).apply {
    await.timeout(timeout.time.duration)
        .pollInterval(pollInterval.time.duration)
        .ignoreException(WebDriverException::class.java)
        .until { isElementPresent() }
}

fun waitForPageLoad(
    driver: WebDriver,
    timeout: Timeout = Timeout.S_30,
    pollInterval: Timeout = Timeout.MS_250
): DriverWait = DriverWait(driver).apply {
    await.timeout(timeout.time.duration)
        .pollInterval(pollInterval.time.duration)
        .until { isPageLoaded() }
}
