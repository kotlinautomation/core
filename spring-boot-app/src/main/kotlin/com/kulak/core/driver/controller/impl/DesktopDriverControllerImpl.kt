package com.kulak.core.driver.controller.impl

import com.kulak.core.constants.DRIVER_PATH
import com.kulak.core.constants.DriverEndPoints.GET_DRIVER
import com.kulak.core.constants.DriverEndPoints.GET_DRIVER_ELEMENTS
import com.kulak.core.constants.DriverEndPoints.LOGS
import com.kulak.core.constants.DriverEndPoints.OPEN_PAGE
import com.kulak.core.constants.DriverEndPoints.QUIT_DRIVER
import com.kulak.core.constants.DriverEndPoints.START_DRIVER
import com.kulak.core.driver.constants.PAGE_OPENED
import com.kulak.core.driver.constants.WEBDRIVER_NOT_QUITED
import com.kulak.core.driver.constants.WEBDRIVER_QUITED
import com.kulak.core.driver.constants.WEBDRIVER_STARTED
import com.kulak.core.driver.controller.DesktopDriverController
import com.kulak.core.driver.entites.WebDriverCache
import com.kulak.core.driver.entites.WebDriverRepository
import com.kulak.core.driver.entites.WebElementRepository
import com.kulak.core.driver.utils.getBrowserType
import com.kulak.core.driver.utils.tell
import com.kulak.core.driver.utils.waitForPageLoad
import com.kulak.core.entities.actions.Action
import com.kulak.core.entities.actions.ActionResult
import com.kulak.core.entities.actions.ActionUrl
import net.lightbody.bmp.core.har.HarEntry
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@CrossOrigin
@RestController
@RequestMapping("/$DRIVER_PATH")
internal class DesktopDriverControllerImpl @Autowired constructor(
    private val webDriverRepository: WebDriverRepository,
    private val webElementRepository: WebElementRepository
) : DesktopDriverController {
    private var log: Logger = LoggerFactory.getLogger(DesktopDriverControllerImpl::class.java)

    @PostMapping(
        path = [QUIT_DRIVER],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    override fun quitDriver(@RequestBody action: Action): ActionResult =
        action.run {
            webDriverRepository.let {
                when {
                    it.existsBySessionId(sessionId) ->
                        it.findBySessionId(sessionId).run {
                            destroyDriver()
                            it.delete(this)
                            ActionResult(sessionId, WEBDRIVER_QUITED) tell log
                        }
                    else -> ActionResult(sessionId, WEBDRIVER_NOT_QUITED)
                }
            }
        }

    @GetMapping(
        path = [START_DRIVER]
    )
    override fun startDriver(
        @RequestParam(required = false) capabilities: Map<String, String>
    ): ActionResult =
        capabilities.run {
            WebDriverCache(getBrowserType(this).browserName).let {
                it.initDriver(this)
                webDriverRepository.save(it)
                ActionResult(it.sessionId, WEBDRIVER_STARTED) tell log
            }
        }

    @PostMapping(
        path = [OPEN_PAGE],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    override fun openPage(@RequestBody actionUrl: ActionUrl): ActionResult =
        actionUrl.run {
            webDriverRepository.findBySessionId(sessionId).driver.let {
                it.navigate().to(url)
                waitForPageLoad(it)
                ActionResult(sessionId, PAGE_OPENED) tell log
            }
        }

    @PostMapping(
        path = [LOGS],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    override fun getLogs(@RequestBody action: Action): List<HarEntry> =
        action.run {
            webDriverRepository.findBySessionId(sessionId).harLogs
        }

    @GetMapping("/")
    override fun findAll() = webDriverRepository.findAllByOrderByStartedAtDesc()

    @GetMapping(GET_DRIVER)
    override fun findOne(@PathVariable sessionId: String) = webDriverRepository.findBySessionId(sessionId)

    @GetMapping(GET_DRIVER_ELEMENTS)
    override fun findElements(@PathVariable sessionId: String) = webElementRepository.findAll()
        .filter { webDriverRepository.findBySessionId(sessionId) == it.webDriverCache }
}
