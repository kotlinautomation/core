package com.kulak.core.driver.entites

import com.fasterxml.jackson.annotation.JsonIgnore
import com.kulak.core.driver.utils.createWebDriver
import net.lightbody.bmp.BrowserMobProxy
import net.lightbody.bmp.BrowserMobProxyServer
import net.lightbody.bmp.client.ClientUtil
import net.lightbody.bmp.core.har.HarEntry
import org.openqa.selenium.Proxy
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.remote.CapabilityType
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.LocalDateTime
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
class WebDriverCache(
    var browserName: String,
    var startedAt: LocalDateTime = LocalDateTime.now(),
    var sessionId: String = UUID.randomUUID().toString(),
    @JsonIgnore @Id @GeneratedValue var id: Long? = null
) {
    @get:[JsonIgnore]
    val driver: WebDriver
        get() = WebDriverManager.getDriver(sessionId)

    @get:[JsonIgnore]
    val harLogs: MutableList<HarEntry>
        get() = WebDriverManager.getHarLogs(sessionId)

    infix fun initDriver(request: Map<String, Any>) {
        mutableMapOf(
            CapabilityType.PROXY to WebDriverManager.initProxy(sessionId),
            CapabilityType.SUPPORTS_JAVASCRIPT to true,
            CapabilityType.ACCEPT_SSL_CERTS to true,
            CapabilityType.ACCEPT_INSECURE_CERTS to true
        ).also {
            it.putAll(request)
            createWebDriver(it).apply {
                manage().window().maximize()
                WebDriverManager.initWebDriver(sessionId, this)
            }
        }
    }

    fun destroyDriver() = WebDriverManager.removeDriver(sessionId)
}

@Entity
class WebElementCache(
    @JsonIgnore @ManyToOne var webDriverCache: WebDriverCache,
    var locator: String,
    @JsonIgnore @Id @GeneratedValue var id: Long? = null
) {
    @get:[JsonIgnore]
    val element: WebElement
        get() = WebElementManager.getElement(webDriverCache.sessionId, locator)

    fun initElement(webElement: WebElement) =
        WebElementManager.initElement(webDriverCache.sessionId, locator, webElement)
}

private object WebDriverManager {
    private var log: Logger = LoggerFactory.getLogger(WebDriverManager::class.java)

    private val drivers = hashMapOf<String, WebDriver>()
    private val proxies = hashMapOf<String, BrowserMobProxy>()

    fun initWebDriver(sessionId: String, driver: WebDriver) {
        drivers[sessionId] = driver
        log.info("Webdriver session $sessionId was inited.")
    }

    infix fun getDriver(sessionId: String): WebDriver = drivers[sessionId]!!

    infix fun removeDriver(sessionId: String): Unit = sessionId.let {
        getDriver(it).quit()
        drivers.remove(it)
        log.info("Webdriver session $it was destroyed.")
        WebElementManager.removeElements(it)
        removeProxy(it)
    }

    infix fun getHarLogs(sessionId: String): MutableList<HarEntry> {
        return proxies[sessionId]!!.har.log.entries
    }

    infix fun initProxy(sessionId: String): Proxy = BrowserMobProxyServer().let {
        it.setTrustAllServers(true)
        it.start()
        val seleniumProxy: Proxy = ClientUtil.createSeleniumProxy(it)
        proxies[sessionId] = it
        it.newHar()
        seleniumProxy
    }

    private infix fun removeProxy(sessionId: String) =
        proxies[sessionId]!!.run {
            stop()
            proxies.remove(sessionId)
        }
}

private object WebElementManager {
    private val webElementMap = mutableMapOf<String, MutableMap<String, WebElement>>()

    fun initElement(sessionId: String, locator: String, webElement: WebElement) =
        webElementMap.run { set(sessionId, mutableMapOf(locator to webElement)) }

    infix fun removeElements(sessionId: String) =
        webElementMap.run { remove(sessionId) }

    fun getElement(sessionId: String, locator: String): WebElement =
        webElementMap[sessionId]!!.getValue(locator)
}
