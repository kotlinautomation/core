package com.kulak.core.driver.entites

import org.springframework.data.repository.CrudRepository

interface WebElementRepository : CrudRepository<WebElementCache, String> {
    fun findByLocator(locator: String): WebElementCache
}

interface WebDriverRepository : CrudRepository<WebDriverCache, String> {
    fun existsBySessionId(sessionId: String): Boolean
    fun findBySessionId(sessionId: String): WebDriverCache
    fun findAllByOrderByStartedAtDesc(): Iterable<WebDriverCache>
}
