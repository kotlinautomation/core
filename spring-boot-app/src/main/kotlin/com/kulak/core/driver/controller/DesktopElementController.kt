package com.kulak.core.driver.controller

import com.kulak.core.driver.entites.WebElementCache
import com.kulak.core.entities.actions.ActionResult
import com.kulak.core.entities.actions.ElementAction
import com.kulak.core.entities.actions.ElementActionWithText

interface DesktopElementController {
    fun waitForElementPresent(elementAction: ElementAction): ActionResult
    fun isElementPresent(elementAction: ElementAction): ActionResult
    fun click(elementAction: ElementAction): ActionResult
    fun type(elementActionWithText: ElementActionWithText): ActionResult
    fun getText(elementAction: ElementAction): ActionResult
    fun findAll(): Iterable<WebElementCache>
    fun findOne(locator: String): WebElementCache
}
