package com.kulak.core.driver.controller.impl

import com.kulak.core.constants.ELEMENT_PATH
import com.kulak.core.constants.ElementEndPoints.CLICK
import com.kulak.core.constants.ElementEndPoints.GET_ELEMENT
import com.kulak.core.constants.ElementEndPoints.GET_TEXT
import com.kulak.core.constants.ElementEndPoints.IS_ELEMENT_PRESENT
import com.kulak.core.constants.ElementEndPoints.TYPE
import com.kulak.core.constants.ElementEndPoints.WAIT_FOR_ELEMENT_PRESENT
import com.kulak.core.driver.constants.ELEMENT_CLICKED
import com.kulak.core.driver.constants.TEXT_TYPED
import com.kulak.core.driver.constants.Timeout
import com.kulak.core.driver.constants.WAITING_FINISHED
import com.kulak.core.driver.controller.DesktopElementController
import com.kulak.core.driver.entites.WebDriverRepository
import com.kulak.core.driver.entites.WebElementCache
import com.kulak.core.driver.entites.WebElementRepository
import com.kulak.core.driver.utils.by
import com.kulak.core.driver.utils.tell
import com.kulak.core.driver.utils.waitForElementPresent
import com.kulak.core.driver.utils.waitForPageLoad
import com.kulak.core.entities.actions.ActionResult
import com.kulak.core.entities.actions.ElementAction
import com.kulak.core.entities.actions.ElementActionWithText
import org.openqa.selenium.interactions.Actions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin
@RestController
@RequestMapping("/$ELEMENT_PATH")
internal class DesktopElementControllerImpl @Autowired constructor(
    private val webDriverRepository: WebDriverRepository,
    private val webElementRepository: WebElementRepository
) : DesktopElementController {
    private var log: Logger = LoggerFactory.getLogger(DesktopElementControllerImpl::class.java)

    @PostMapping(
        path = [WAIT_FOR_ELEMENT_PRESENT],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    override fun waitForElementPresent(@RequestBody elementAction: ElementAction): ActionResult =
        elementAction.run {
            webDriverRepository.findBySessionId(sessionId).let {
                if (waitForElementPresent(it.driver, locator).isElementPresent()) {
                    val element = it.driver.findElement(locator.by)
                    val webElementCache = WebElementCache(it, locator.value)
                    webElementCache.initElement(element)
                    webElementRepository.save(webElementCache)
                }
                ActionResult(sessionId, WAITING_FINISHED) tell log
            }
        }

    @PostMapping(
        path = [IS_ELEMENT_PRESENT],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    override fun isElementPresent(@RequestBody elementAction: ElementAction): ActionResult =
        elementAction.run {
            webDriverRepository.findBySessionId(sessionId).let {
                val isPresent = waitForElementPresent(
                    it.driver,
                    locator,
                    Timeout.S_2,
                    Timeout.MS_250
                ).isElementPresent()
                ActionResult(sessionId, isPresent.toString())
            }
        }

    @PostMapping(
        path = [CLICK],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    override fun click(@RequestBody elementAction: ElementAction): ActionResult =
        elementAction.run {
            webDriverRepository.findBySessionId(sessionId).driver.let {
                val element = webElementRepository.findByLocator(locator.value).element
                Actions(it)
                    .moveToElement(element)
                    .pause(Timeout.MS_2000.timeL)
                    .click()
                    .build()
                    .perform()
                waitForPageLoad(it)
                ActionResult(sessionId, ELEMENT_CLICKED) tell log
            }
        }

    @PostMapping(
        path = [TYPE],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    override fun type(@RequestBody elementActionWithText: ElementActionWithText): ActionResult =
        elementActionWithText.run {
            webElementRepository.findByLocator(locator.value).element.let {
                it.sendKeys(text)
                ActionResult(sessionId, TEXT_TYPED) tell log
            }
        }

    @PostMapping(
        path = [GET_TEXT],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    override fun getText(@RequestBody elementAction: ElementAction): ActionResult =
        elementAction.run {
            webElementRepository.findByLocator(locator.value).element.let {
                ActionResult(sessionId, it.text)
            }
        }

    @GetMapping("/")
    override fun findAll(): Iterable<WebElementCache> = webElementRepository.findAll()

    @GetMapping(GET_ELEMENT)
    override fun findOne(@PathVariable locator: String) = webElementRepository.findByLocator(locator)
}
