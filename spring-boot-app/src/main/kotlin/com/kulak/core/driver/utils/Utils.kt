package com.kulak.core.driver.utils

import com.kulak.core.constants.BROWSER_NAME
import com.kulak.core.entities.browsers.BrowserType
import org.openqa.selenium.WebDriver
import java.util.Locale

fun createWebDriver(
    request: Map<String, Any>
): WebDriver = getBrowserType(request).createDriver(request)

fun getBrowserType(request: Map<String, Any>): BrowserType = when {
    request.isEmpty() -> BrowserType.CHROME
    !request.containsKey(BROWSER_NAME) -> BrowserType.CHROME
    else -> BrowserType.valueOf((request[BROWSER_NAME] as String).toUpperCase(Locale.ENGLISH))
}
