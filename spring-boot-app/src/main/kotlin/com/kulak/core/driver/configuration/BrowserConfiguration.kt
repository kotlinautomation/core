package com.kulak.core.driver.configuration

import com.kulak.core.driver.entites.WebDriverRepository
import com.kulak.core.driver.entites.WebElementRepository
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class BrowserConfiguration {
    @Bean
    open fun databaseInitializer(
        webDriverRepository: WebDriverRepository,
        webElementRepository: WebElementRepository
    ) = ApplicationRunner {
    }
}
