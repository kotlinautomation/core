package com.kulak.core.driver.constants

import com.soywiz.klock.TimeSpan
import com.soywiz.klock.milliseconds
import com.soywiz.klock.seconds

enum class Timeout(var time: TimeSpan) {
    S_2(2.seconds),
    S_10(10.seconds),
    S_30(30.seconds),
    MS_250(250.milliseconds),
    MS_500(500.milliseconds),
    MS_2000(2000.milliseconds);

    val timeL get() = time.millisecondsLong
}

const val WEBDRIVER_STARTED = "Webdriver was successfully started."
const val WEBDRIVER_NOT_STARTED = "Webdriver wasn't started cause \"debug\" session already is present."
const val WEBDRIVER_QUITED = "Webdriver was successfully quited."
const val WEBDRIVER_NOT_QUITED = "Webdriver was not quited."
const val WAITING_FINISHED = "Waiting for element was finished."
const val ELEMENT_CLICKED = "Element was clicked."
const val TEXT_TYPED = "Text was typed to element."
const val PAGE_OPENED = "Page was opened."
