package com.kulak.core.driver.controller

import com.kulak.core.driver.entites.WebDriverCache
import com.kulak.core.driver.entites.WebElementCache
import com.kulak.core.entities.actions.Action
import com.kulak.core.entities.actions.ActionResult
import com.kulak.core.entities.actions.ActionUrl
import net.lightbody.bmp.core.har.HarEntry

interface DesktopDriverController {
    fun quitDriver(action: Action): ActionResult
    fun startDriver(capabilities: Map<String, String>): ActionResult
    fun openPage(actionUrl: ActionUrl): ActionResult
    fun getLogs(action: Action): List<HarEntry>
    fun findAll(): Iterable<WebDriverCache>
    fun findOne(sessionId: String): WebDriverCache
    fun findElements(sessionId: String): List<WebElementCache>
}
