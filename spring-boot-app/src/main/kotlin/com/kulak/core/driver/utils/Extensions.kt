package com.kulak.core.driver.utils

import com.kulak.core.entities.actions.ActionResult
import com.kulak.core.entities.actions.Locator
import com.kulak.core.entities.actions.LocatorType
import com.kulak.core.entities.browsers.BrowserType
import com.soywiz.klock.TimeSpan
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.MutableCapabilities
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.slf4j.Logger
import java.time.Duration
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

val Locator.by: By
    get() = when (type) {
        LocatorType.XPATH -> By.xpath(value)
        LocatorType.CSS -> By.cssSelector(value)
        LocatorType.ID -> By.id(value)
    }

infix fun ActionResult.tell(log: Logger) = this.also { log.info(message) }

fun WebDriver.executeScript(
    script: String,
    vararg args: JvmType.Object
) = (this as JavascriptExecutor)
    .executeScript(script, args)

inline val TimeSpan.duration: Duration get() = Duration.ofMillis(millisecondsLong)

val BrowserType.capabilities
    get() = when (this) {
        BrowserType.CHROME -> ChromeOptions()
        BrowserType.FIREFOX -> FirefoxOptions()
    }

fun BrowserType.getDriver(capabilities: MutableCapabilities): WebDriver = when (this) {
    BrowserType.CHROME -> ChromeDriver(capabilities as ChromeOptions)
    BrowserType.FIREFOX -> FirefoxDriver(capabilities as FirefoxOptions)
}

infix fun BrowserType.createDriver(request: Map<String, Any>): WebDriver = capabilities.also {
    request.forEach { (key, value) ->
        it.setCapability(key, value)
    }
}.let {
    getDriver(it)
}
