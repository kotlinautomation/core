package com.kulak.core

import com.kulak.core.entities.browsers.BrowserType
import com.kulak.core.utils.WebAssertions.Companion.simpleWebAssert
import com.kulak.core.utils.getDriver
import com.kulak.core.utils.getDrivers
import com.kulak.core.utils.startQuitDriver
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ApplicationTests(
    @Autowired val restTemplate: TestRestTemplate
) {
    @Test
    fun `Context loads`() {
        // Tests context loading
    }

    @Test
    fun `Driver starts`() = restTemplate.startQuitDriver(::simpleWebAssert, ::simpleWebAssert)

    @Test
    fun `Drivers endpoint works as should`() = restTemplate.startQuitDriver { response ->
        restTemplate.getDrivers().also {
            assertThat(it.body!!.size).isEqualTo(1)
            assertThat(it.body!![0].sessionId).isEqualTo(response.body!!.sessionId)
            assertThat(it.body!![0].browserName).isEqualToIgnoringCase(BrowserType.CHROME.browserName)
        }
    }

    @Test
    fun `Driver endpoint works as should`() = restTemplate.startQuitDriver { response ->
        restTemplate.getDriver(response.body!!.sessionId).also {
            assertThat(it.body!!.sessionId).isEqualTo(response.body!!.sessionId)
            assertThat(it.body!!.browserName).isEqualToIgnoringCase(BrowserType.CHROME.browserName)
        }
    }
}
