package com.kulak.core.utils

import org.assertj.core.api.Assertions
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

class WebAssertions : Assertions() {
    companion object {
        fun simpleWebAssert(responseEntity: ResponseEntity<*>) {
            assertThat(responseEntity.statusCode).isEqualTo(HttpStatus.OK)
            assertThat(responseEntity.body).isNotNull
        }
    }
}
