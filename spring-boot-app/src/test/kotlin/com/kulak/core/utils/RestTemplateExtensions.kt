package com.kulak.core.utils

import com.kulak.core.constants.BROWSER_NAME
import com.kulak.core.constants.DRIVER_PATH
import com.kulak.core.constants.DriverEndPoints.QUIT_DRIVER
import com.kulak.core.constants.DriverEndPoints.START_DRIVER
import com.kulak.core.entities.WebDriverCacheCommon
import com.kulak.core.entities.actions.Action
import com.kulak.core.entities.actions.ActionResult
import com.kulak.core.entities.browsers.BrowserType
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.ResponseEntity

private fun TestRestTemplate.startDriver(
    browserType: BrowserType = BrowserType.CHROME
): ResponseEntity<ActionResult> = this.getForEntity(
    "/$DRIVER_PATH$START_DRIVER",
    ActionResult::class.java,
    mapOf(BROWSER_NAME to browserType)
)

private fun TestRestTemplate.quitDriver(
    sessionId: String
): ResponseEntity<ActionResult> = this.postForEntity(
    "/$DRIVER_PATH$QUIT_DRIVER",
    Action(sessionId),
    ActionResult::class.java
)

internal fun TestRestTemplate.getDrivers(): ResponseEntity<WebDriverCaches> = this.getForEntity(
    "/$DRIVER_PATH/",
    WebDriverCaches::class.java
)

fun TestRestTemplate.getDriver(
    sessionId: String
): ResponseEntity<WebDriverCacheCommon> = this.getForEntity(
    "/$DRIVER_PATH/$sessionId",
    WebDriverCacheCommon::class.java
)

internal class WebDriverCaches : ArrayList<WebDriverCacheCommon>()

fun TestRestTemplate.startQuitDriver(
    startResponseAction: (ResponseEntity<ActionResult>) -> (Unit)
): Unit = this.let {
    val startResponse = startDriver()
    startResponseAction.invoke(startResponse)
    quitDriver(startResponse.body!!.sessionId)
}

fun TestRestTemplate.startQuitDriver(
    startResponseAction: (ResponseEntity<ActionResult>) -> (Unit),
    quitResponseAction: (ResponseEntity<ActionResult>) -> (Unit)
): Unit = this.let {
    val startResponse = startDriver()
    startResponseAction.invoke(startResponse)
    val quitResponse = quitDriver(startResponse.body!!.sessionId)
    quitResponseAction.invoke(quitResponse)
}
